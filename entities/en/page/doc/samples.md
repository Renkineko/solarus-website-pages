[container]
##Titles
#Title level 1
##Title level 2
###Title level 3
####Title level 4
[space height="50"][/space]
## Paragraphs
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[space height="50"][/space]
##Alerts
[alert type="primary" dismiss="true" title="Primary"]
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="secondary" dismiss="true" title="Secondary"]
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="info" dismiss="true" title="Info"]
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="success" dismiss="true" title="Success"]
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="warning" dismiss="true" title="Warning"]
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="danger" dismiss="true" title="Danger"]
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="light" dismiss="true" title="Light"]
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="dark" dismiss="true" title="Dark"]
Lorem ipsum dolor sit amet, **consectetur** adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="primary" dismiss="true" icon-model="fab" icon="windows"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="primary" dismiss="true" title="With title"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="secondary" dismiss="true" title="With icon and title" icon-model="fab" icon="windows"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[alert type="primary" title="Without dismiss action"]
Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
[/alert]
[space height="50"][/space]
##Badges
[badge type="primary"]
Primary
[/badge]
[badge type="secondary"]
Primary
[/badge]
[badge type="info"]
Info
[/badge]
[badge type="success"]
Success
[/badge]
[badge type="warning"]
Warning
[/badge]
[badge type="danger"]
Danger
[/badge]
[badge type="light"]
Light
[/badge]
[badge type="dark"]
Dark
[/badge]
[badge type="primary" icon-model="fab" icon="windows"]
With icon
[/badge]
[space height="50"][/space]
##Buttons
[button type="primary"]
Primary
[/button]
[button type="secondary"]
Secondary
[/button]
[button type="info"]
Info
[/button]
[button type="success"]
Success
[/button]
[button type="warning"]
Warning
[/button]
[button type="danger"]
Danger
[/button]
[button type="light"]
Light
[/button]
[button type="dark"]
Dark
[/button]
[space][/space]
[button type="primary" outline="true"]
Primary outline
[/button]
[button type="secondary" outline="true"]
Secondary outline
[/button]
[button type="info" outline="true"]
Info outline
[/button]
[button type="success" outline="true"]
Success outline
[/button]
[space][/space]
[button type="warning" outline="true"]
Warning outline
[/button]
[button type="danger" outline="true"]
Danger outline
[/button]
[button type="light" outline="true]
Light outline
[/button]
[button type="dark" outline="true]
Dark outline
[/button]
[space][/space]
[button type="primary" size="lg"]
Large
[/button]
[button type="primary" size="md"]
Medium
[/button]
[button type="primary" size="sm"]
Small
[/button]
[space][/space]
[button type="primary" icon-model="fab" icon="linux"]
With icon
[/button]
[button type="secondary" icon-model="fab" icon="apple" outline="true"]
With icon outline
[/button]
[space height="50"][/space]
##Icons
[icon icon="apple" model="fab" type="primary"][/icon]
[icon icon="apple" model="fab" type="secondary"][/icon]
[icon icon="apple" model="fab" type="info"][/icon]
[icon icon="apple" model="fab" type="success"][/icon]
[icon icon="apple" model="fab" type="warning"][/icon]
[icon icon="apple" model="fab" type="danger"][/icon]
[icon icon="apple" model="fab" type="light"][/icon]
[icon icon="apple" model="fab" type="dark"][/icon]
[space][/space]
[icon icon="apple" model="fab" size="2x"][/icon]
[icon icon="apple" model="fab" size="3x"][/icon]
[icon icon="apple" model="fab" size="4x"][/icon]
[icon icon="apple" model="fab" size="5x"][/icon]
[icon icon="apple" model="fab" size="6x"][/icon]
[icon icon="apple" model="fab" size="7x"][/icon]
[icon icon="apple" model="fab" size="8x"][/icon]
[icon icon="apple" model="fab" size="9x"][/icon]
[icon icon="apple" model="fab" size="10x"][/icon]
[icon icon="apple" model="fab" bordered="true"][/icon]
[icon icon="apple" model="fab" mask="circle"][/icon]
[space height="50"][/space]
##Progressbars
[progressbar type="primary" width="100"]
Primary
[/progressbar]
[space][/space]
[progressbar type="secondary" width="90"]
Secondary
[/progressbar]
[space][/space]
[progressbar type="info" width="80"]
Info
[/progressbar]
[space][/space]
[progressbar type="success" width="70"]
Success
[/progressbar]
[space][/space]
[progressbar type="warning" width="60"]
Warning
[/progressbar]
[space][/space]
[progressbar type="danger" width="50"]
Danger
[/progressbar]
[space][/space]
[progressbar type="light" width="50"]
Light
[/progressbar]
[space][/space]
[progressbar type="dark" width="50"]
Dark
[/progressbar]
[space][/space]
[progressbar type="primary" striped="true" width="40"]
Striped
[/progressbar]
[space][/space]
[progressbar type="primary" striped="true" animated="true" width="30"]
Striped and animated
[/progressbar]
[/container]